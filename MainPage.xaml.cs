using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=234238 上有介绍

namespace 分享
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 在此页将要在 Frame 中显示时进行调用。
        /// </summary>
        /// <param name="e">描述如何访问此页的事件数据。Parameter
        /// 属性通常用于配置页。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DataTransferManager.GetForCurrentView().DataRequested += OnDataRequested;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //打开右侧分享栏
            DataTransferManager.ShowShareUI();
        }

        //分享源代码开始
        void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {

            var request = args.Request;

            string title = this.title.Text;
            string url = this.url.Text;
            string content = this.content.Text;

            request.Data.Properties.Title = title;
            request.Data.Properties.Description = "";

            // Share recipe text
            var recipe = content;

            recipe += ("\r\n\r\n" + url);
            request.Data.SetText(recipe);
        }
    }
}
